﻿using System;
using System.Collections.Generic;
using System.Text;
using Tecan.Sila2;

namespace Hsrm.EdgeGateway.Contracts
{
    /// <summary>
    /// Denotes a feature that allows to monitor the communication to connected servers
    /// </summary>
    [SilaFeature(true, "gateway")]
    public interface IGatewayMonitoringService
    {
        /// <summary>
        /// Listens to messages of servers that follow the specified pattern
        /// </summary>
        /// <param name="serverPattern">The pattern of the server UUID that is followed, can contain wildcards like *</param>
        /// <returns>A command that returns the actual monitoring messages as intermediate results</returns>
        IIntermediateObservableCommand<GatewayMonitoringMessage> ListenToServerMessages(string serverPattern);
    }
}
