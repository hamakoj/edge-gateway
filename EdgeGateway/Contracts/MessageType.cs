﻿namespace Hsrm.EdgeGateway.Contracts
{
    public enum MessageType
    {
        CommandInvocation,
        PropertyRead
    }
}