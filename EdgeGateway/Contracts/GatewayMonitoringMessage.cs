﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hsrm.EdgeGateway.Contracts
{
    public class GatewayMonitoringMessage
    {
        public MessageType Type { get; }

        public string ServerUuid { get; }

        public string CommandId { get; }
    }
}
