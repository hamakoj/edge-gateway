﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hsrm.EdgeGateway.Contracts
{
    public class InvalidServerUuidException : Exception
    {
        public InvalidServerUuidException(string message) : base(message)
        {
        }
    }
}
