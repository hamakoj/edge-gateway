﻿using Hsrm.EdgeGateway.GatewayService;
using Tecan.Sila2;

namespace Hsrm.EdgeGateway.Contracts
{
    public interface IServerResolver
    {
        ConnectedServer ResolveServer(string serverUuid);
    }
}
