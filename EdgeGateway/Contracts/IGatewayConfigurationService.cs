﻿using System;
using System.Collections.Generic;
using System.Text;
using Tecan.Sila2;

namespace Hsrm.EdgeGateway.Contracts
{
    /// <summary>
    /// Denotes a feature to configure the gateway
    /// </summary>
    [SilaFeature(true, "gateway")]
    public interface IGatewayConfigurationService
    {
        /// <summary>
        /// Attempts to connect to the server with the specified host name and port
        /// </summary>
        /// <param name="host">The host name</param>
        /// <param name="port">The port of the server to connect</param>
        void AddServer(string host, int port);

        /// <summary>
        /// Removes the server with the given server unique identifier
        /// </summary>
        /// <param name="serverUuid"></param>
        void RemoveServer(string serverUuid);
    }
}
