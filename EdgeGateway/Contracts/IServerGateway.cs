﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tecan.Sila2;

namespace Hsrm.EdgeGateway.Contracts
{
    public interface IServerGateway
    {
        void AddServer(ServerData serverData);

        bool RemoveServer(Guid serverUuid);
    }
}
