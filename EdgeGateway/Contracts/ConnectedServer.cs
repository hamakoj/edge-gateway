﻿using Hsrm.EdgeGateway.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tecan.Sila2;
using Tecan.Sila2.Client;
using Tecan.Sila2.Server;

namespace Hsrm.EdgeGateway.GatewayService
{
    public class ConnectedServer 
    {
        public ConnectedServer(ServerData server, IClientExecutionManager executionManager)
        {
            Server = server;
            ExecutionManager = executionManager;
            Info = new Server(server.Config.Uuid.ToString(), server.Config.Name, server.Info.Type, server.Info.Version, server.Info.Description, server.Info.VendorUri, server.Features.Select(f => f.FullyQualifiedIdentifier).ToList());

            _metadatas = (from f in server.Features
                          from metadat in f.Items.OfType<FeatureMetadata>()
                          select f.GetFullyQualifiedIdentifier(metadat)).ToList();
        }

        private readonly List<string> _metadatas = new List<string>();

        public ServerData Server { get; }

        public Server Info { get; }

        public IClientExecutionManager ExecutionManager { get; }

        public IDictionary<string, byte[]> CreateAdditionalMetadata(IMetadataRepository metadata)
        {
            if (_metadatas.Count == 0) return null;
            var result = new Dictionary<string, byte[]>();
            foreach (var key in _metadatas)
            {
                if (metadata.TryGetMetadata(key, out var value))
                {
                    result.Add(key, value);
                }
            }
            return result;
        }
    }
}
