﻿using Hsrm.EdgeGateway.Contracts;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tecan.Sila2;

namespace Hsrm.EdgeGateway.Monitoring
{
    [Export(typeof(IGatewayMonitoringService))]
    [Export(typeof(IGatewayMonitor))]
    [PartCreationPolicy(CreationPolicy.Shared)]
    internal class GatewayMonitor : IGatewayMonitoringService, IGatewayMonitor
    {
        public IIntermediateObservableCommand<GatewayMonitoringMessage> ListenToServerMessages(string serverPattern)
        {
            throw new NotImplementedException();
        }

        public void SendMonitoringMessage(GatewayMonitoringMessage message)
        {
            throw new NotImplementedException();
        }
    }
}
