﻿using Hsrm.EdgeGateway.Contracts;
using Hsrm.EdgeGateway.GatewayService;
using Microsoft.AspNetCore.Mvc;
using Tecan.Sila2;

namespace Hsrm.EdgeGateway.Controllers
{
    [ApiController]
    [Route("/api/gateway")]
    public class GatewayController : Controller
    {
        private readonly IGatewayService _gatewayService;

        public GatewayController(IGatewayService gatewayService)
        {
            _gatewayService = gatewayService;
        }

        [HttpGet]
        public IEnumerable<ServerInfo> GetConnectedServers()
        {
            return (_gatewayService.ConnectedServers ?? Enumerable.Empty<Server>()).Select(s =>
                new ServerInfo(s.ServerUuid, s.Name, s.Type, s.Description, s.Features));
        }

        [HttpPost]
        public IActionResult UpdateServers([FromBody] DiscoverySpec discovery)
        {
            if (discovery.timeout <= 0)
            {
                return BadRequest();
            }
            Task.Run(() => _gatewayService.ScanNetwork(discovery.timeout));
            return Ok();
        }

        [HttpPut]
        public IActionResult AddServer([FromBody] ServerSpec server)
        {
            _gatewayService.AddServer(server.Host, server.Port);
            return Ok();
        }
    }

    public record ServerInfo(
        string serveruuid,
        string serverName,
        string serverType,
        string description,
        IReadOnlyList<string> features);

    public record DiscoverySpec(int timeout);

    public record ServerSpec(string Host, int Port);
}
