﻿using Hsrm.EdgeGateway.Contracts;
using Microsoft.AspNetCore.Mvc;
using Tecan.Sila2.Server.ConnectionConfiguration;

namespace Hsrm.EdgeGateway.Controllers
{
    [ApiController]
    [Route("/api/connectionConfiguration")]
    public class ConnectionConfigurationController : Controller
    {
        private readonly IConnectionConfigurationService _connectionConfigurationService;

        public ConnectionConfigurationController(IConnectionConfigurationService connectionConfigurationService)
        {
            _connectionConfigurationService = connectionConfigurationService;
        }

        [HttpGet]
        public IEnumerable<ClientConfiguration> GetConfiguredClients()
        {
            return _connectionConfigurationService.ConfiguredSiLAClients
                .Select(c => new ClientConfiguration(c.ClientName, c.SiLAClientHost, (int)c.SiLAClientPort))
                .ToList();
        }

        [HttpPut]
        public IActionResult AddConfiguredClient( [FromBody] ClientConfiguration client, bool persist = true)
        {
            try
            {
                if (!_connectionConfigurationService.ServerInitiatedConnectionModeStatus)
                {
                    _connectionConfigurationService.EnableServerInitiatedConnectionMode();
                }
                _connectionConfigurationService.ConnectSiLAClient(client.ClientName, client.Host, client.Port, persist);
                return Ok();
            }
            catch (Exception ex)
            {
                return Problem(ex.Message, statusCode: 400);
            }
        }

        [HttpDelete]
        public IActionResult RemoveConfiguredClient(string clientName, bool remove = true)
        {
            _connectionConfigurationService.DisconnectSiLAClient(clientName, remove);
            return Ok();
        }

        public record ClientConfiguration(string ClientName, string Host, int Port);
    }
}
