//------------------------------------------------------------------------------
// <auto-generated>
//     Dieser Code wurde von einem Tool generiert.
//     Laufzeitversion:4.0.30319.42000
//
//     Änderungen an dieser Datei können falsches Verhalten verursachen und gehen verloren, wenn
//     der Code erneut generiert wird.
// </auto-generated>
//------------------------------------------------------------------------------

using Hsrm.EdgeGateway.Contracts;
using System;
using Tecan.Sila2;


namespace Hsrm.EdgeGateway.GatewayMonitoringService
{
    
    
    /// <summary>
    /// Data transfer object for the request of the Listen To Server Messages command
    /// </summary>
    [ProtoBuf.ProtoContractAttribute()]
    public class ListenToServerMessagesRequestDto : Tecan.Sila2.ISilaTransferObject, Tecan.Sila2.ISilaRequestObject
    {
        
        private Tecan.Sila2.StringDto _serverPattern;
        
        /// <summary>
        /// Create a new instance
        /// </summary>
        public ListenToServerMessagesRequestDto()
        {
        }
        
        /// <summary>
        /// Create a new instance
        /// </summary>
        /// <param name="store">An object to organize binaries.</param>
        /// <param name="serverPattern">The pattern of the server UUID that is followed, can contain wildcards like *</param>
        public ListenToServerMessagesRequestDto(string serverPattern, Tecan.Sila2.IBinaryStore store)
        {
            ServerPattern = new Tecan.Sila2.StringDto(serverPattern, store);
        }
        
        /// <summary>
        /// The pattern of the server UUID that is followed, can contain wildcards like *
        /// </summary>
        [ProtoBuf.ProtoMemberAttribute(1)]
        public Tecan.Sila2.StringDto ServerPattern
        {
            get
            {
                return _serverPattern;
            }
            set
            {
                _serverPattern = value;
            }
        }
        
        /// <summary>
        /// Gets the command identifier for this command
        /// </summary>
        /// <returns>The fully qualified command identifier</returns>
        public string CommandIdentifier
        {
            get
            {
                return "org.silastandard/gateway/GatewayMonitoringService/v1/Command/ListenToServerMessag" +
                    "es";
            }
        }
        
        /// <summary>
        /// Validates the given request object
        /// </summary>
        public void Validate()
        {
            Argument.Validate(ServerPattern, "serverPattern");
        }
        
        /// <summary>
        /// Validates the contents of this transfer object
        /// </summary>
        /// <returns>A validation error or null, if no validation error occurred.</returns>
        public string GetValidationErrors()
        {
            return ("" + Argument.Require(ServerPattern, "serverPattern"));
        }
    }
    
    /// <summary>
    /// Data transfer object for the intermediate response of the Listen To Server Messages command
    /// </summary>
    [ProtoBuf.ProtoContractAttribute()]
    public class ListenToServerMessagesIntermediateDto : Tecan.Sila2.ISilaTransferObject
    {
        
        private GatewayMonitoringMessageDto _intermediate;
        
        /// <summary>
        /// Create a new instance
        /// </summary>
        public ListenToServerMessagesIntermediateDto()
        {
        }
        
        /// <summary>
        /// Create a new instance
        /// </summary>
        /// <param name="store">An object to organize binaries.</param>
        public ListenToServerMessagesIntermediateDto(GatewayMonitoringMessage intermediate, Tecan.Sila2.IBinaryStore store)
        {
            Intermediate = new GatewayMonitoringMessageDto(intermediate, store);
        }
        
        [ProtoBuf.ProtoMemberAttribute(1)]
        public GatewayMonitoringMessageDto Intermediate
        {
            get
            {
                return _intermediate;
            }
            set
            {
                _intermediate = value;
            }
        }
        
        /// <summary>
        /// Validates the given request object
        /// </summary>
        public void Validate()
        {
            Argument.Validate(Intermediate, "intermediate");
        }
        
        /// <summary>
        /// Validates the contents of this transfer object
        /// </summary>
        /// <returns>A validation error or null, if no validation error occurred.</returns>
        public string GetValidationErrors()
        {
            return ("" + Argument.Require(Intermediate, "intermediate"));
        }
    }
    
    /// <summary>
    /// The data transfer object for Gateway Monitoring Message
    /// </summary>
    [ProtoBuf.ProtoContractAttribute()]
    public class GatewayMonitoringMessageDto : Tecan.Sila2.ISilaTransferObject<GatewayMonitoringMessage>
    {
        
        private InnerStruct _inner;
        
        /// <summary>
        /// Initializes a new instance (to be used by the serializer)
        /// </summary>
        public GatewayMonitoringMessageDto()
        {
            _inner = new InnerStruct();
        }
        
        /// <summary>
        /// Initializes a new data transfer object from the business object
        /// </summary>
        /// <param name="inner">The business object that should be transferred</param>
        /// <param name="store">A component to handle binary data</param>
        public GatewayMonitoringMessageDto(GatewayMonitoringMessage inner, Tecan.Sila2.IBinaryStore store)
        {
            _inner = new InnerStruct(inner, store);
        }
        
        /// <summary>
        /// The actual contents of the data transfer object.
        /// </summary>
        [ProtoBuf.ProtoMemberAttribute(1)]
        public InnerStruct Inner
        {
            get
            {
                return _inner;
            }
            set
            {
                _inner = value;
            }
        }
        
        /// <summary>
        /// Extracts the transferred value
        /// </summary>
        /// <param name="store">The binary store in which to store binary data</param>
        /// <returns>the inner value</returns>
        public GatewayMonitoringMessage Extract(Tecan.Sila2.IBinaryStore store)
        {
            return new GatewayMonitoringMessage();
        }
        
        /// <summary>
        /// Creates the data transfer object from the given object to transport
        /// </summary>
        /// <param name="inner">The object to transfer</param>
        /// <param name="store">An object to store binary data</param>
        public static GatewayMonitoringMessageDto Create(GatewayMonitoringMessage inner, Tecan.Sila2.IBinaryStore store)
        {
            return new GatewayMonitoringMessageDto(inner, store);
        }
        
        /// <summary>
        /// Validates the contents of this transfer object
        /// </summary>
        /// <returns>A validation error or null, if no validation error occurred.</returns>
        public string GetValidationErrors()
        {
            return null;
        }
        
        /// <summary>
        /// Represents the inner structure for actual content
        /// </summary>
        [ProtoBuf.ProtoContractAttribute()]
        public class InnerStruct
        {
            
            /// <summary>
            /// Initializes a new instance (to be used by the serializer)
            /// </summary>
            public InnerStruct()
            {
            }
            
            /// <summary>
            /// Initializes a new data transfer object from the business object
            /// </summary>
            /// <param name="inner">The business object that should be transferred</param>
            /// <param name="store">A component to handle binary data</param>
            public InnerStruct(GatewayMonitoringMessage inner, Tecan.Sila2.IBinaryStore store)
            {
            }
        }
    }
}

