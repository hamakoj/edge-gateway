﻿using Hsrm.EdgeGateway.Contracts;
using Tecan.Sila2;
using Tecan.Sila2.Server;

namespace Hsrm.EdgeGateway.Services
{
    public class FeatureResolver : IFeatureResolver
    {
        private readonly ISiLAServer _silaServer;

        public FeatureResolver(ISiLAServer silaServer)
        {
            _silaServer = silaServer;
        }   

        public Feature? Resolve(string identifier)
        {
            var featuresWithIdentifier =
               (from featureProvider in _silaServer.Features
                where string.Equals(featureProvider.FeatureDefinition.Identifier, identifier, StringComparison.InvariantCultureIgnoreCase)
                select featureProvider.FeatureDefinition).ToList();

            if (featuresWithIdentifier.Count == 1)
            {
                return featuresWithIdentifier[0];
            }

            var identifierConverted = identifier.Replace('_', '/');

            return (from featureProvider in _silaServer.Features
                    where string.Equals(featureProvider.FeatureDefinition.FullyQualifiedIdentifier, identifierConverted, StringComparison.InvariantCultureIgnoreCase)
                    select featureProvider.FeatureDefinition).FirstOrDefault();
        }
    }
}
