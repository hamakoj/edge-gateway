import { Component, OnInit } from '@angular/core';
import {GatewayFeatureService} from "../../../api";

@Component({
  selector: 'app-gateway-feature',
  templateUrl: './gateway-feature.component.html',
  styleUrls: ['./gateway-feature.component.css']
})
export class GatewayFeatureComponent implements OnInit {

  gatewayFeatures: Array<string> = [];
  identifier: string = '';

  constructor(private gatewayFeature: GatewayFeatureService) { }

  ngOnInit(): void {
    this.getGatewayFeature();
    // this.addGatewayFeature(this.identifier);
    this.deleteGatewayFeature(this.identifier);
  }

  getGatewayFeature() {
    this.gatewayFeature.apiGatewayFeaturesGet('body', false, {httpHeaderAccept: "text/json"})
      .subscribe({
        next: (data) => {
          this.gatewayFeatures = data;
          console.log(this.gatewayFeatures);
        },
        error: (error) => {
          console.error('Failed to get gateway feature:', error);
          // Handle the error appropriately (e.g., show an error message)
        }
      });
  }

  addGatewayFeature(identifier: string) {
    this.identifier = identifier;
    console.log(this.identifier);
    this.gatewayFeature.apiGatewayFeaturesPut(this.identifier, 'body', false, {httpHeaderAccept: undefined})
      .subscribe({
        next: (data) => {
          console.log(data);
          return 'gateway feature store successfully ';
        },
        error: (error) => {
          console.error('Failed to save gateway feature:', error);
          // Handle the error appropriately (e.g., show an error message)
        },
        complete: () => {
          this.identifier= '';
        }
      })
    //this.identifier= '';
  }

  deleteGatewayFeature(identifier: string) {
    if(!identifier) {
      console.error('Identifier error');
      return;
    }
    this.gatewayFeature.apiGatewayFeaturesDelete(identifier, 'body', false, {httpHeaderAccept: undefined})
      .subscribe({
        next:() => {
          console.log(identifier);
          console.log('Gateway feature deleted successfully.');
        },
        error: (error) => {
          console.error('Failed to delete gateway feature:', error);
        }
      });
  }



}
