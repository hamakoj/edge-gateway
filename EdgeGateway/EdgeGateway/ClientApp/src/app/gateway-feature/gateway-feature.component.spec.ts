import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GatewayFeatureComponent } from './gateway-feature.component';

describe('GatewayFeatureComponent', () => {
  let component: GatewayFeatureComponent;
  let fixture: ComponentFixture<GatewayFeatureComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GatewayFeatureComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(GatewayFeatureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
