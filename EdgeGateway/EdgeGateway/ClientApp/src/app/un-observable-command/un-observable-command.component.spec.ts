import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UnObservableCommandComponent } from './un-observable-command.component';

describe('UnObservableCommandComponent', () => {
  let component: UnObservableCommandComponent;
  let fixture: ComponentFixture<UnObservableCommandComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UnObservableCommandComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UnObservableCommandComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
