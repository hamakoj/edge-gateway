import { Component, OnInit } from '@angular/core';
import {ConfiguredSiLAClient, ConnectionConfigurationService} from "../../../api";
import {FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-connection-configuration',
  templateUrl: './connection-configuration.component.html',
  styleUrls: ['./connection-configuration.component.css']
})
export class ConnectionConfigurationComponent implements OnInit {
  configuredSilaClient: ConfiguredSiLAClient[] = [];
  configSilaClientForm!: FormGroup;
  clientName: string = '';
  persist: boolean = false;
  remove: boolean = false;
  persistOptions: boolean[] = [true, false];

  submitted = false;

  constructor(private connectionConfigService: ConnectionConfigurationService) { }

  ngOnInit(): void {
    this.configSilaClientForm = new FormGroup({
      clientName: new FormControl('', [Validators.required]),
      siLAClientHost: new FormControl('', [Validators.required]),
      siLAClientPort: new FormControl('', [
        Validators.required,
        Validators.pattern(/^[0-9]\d*$/),
      ]),
    });
    //this.getConnectionConfig();
    // this.updateConnectionConfig();
    //this.deleteConnectionConfig();
  }

  get configSilaClientFormControl() {
    return this.configSilaClientForm.controls;
  }

  getConnectionConfig() {
    this.connectionConfigService
      .apiConnectionConfigurationGet('body', false, {
        httpHeaderAccept: 'text/plain',
      })
      .subscribe((result) => {
        if(result){
          console.log(result);
          this.configuredSilaClient = result;
        }else{
          console.error('Loaded call failed.');
        }
      })
      .unsubscribe();
  }

// This function updates the connection configuration
  updateConnectionConfig() {
    // If the SiLA client details are not entered in the form, show the message and return
    if (this.configSilaClientForm.invalid) {
      console.log('Please fill all the required fields.');
      return;
    }
    // Get the configured SiLA client details from the form
    const configuredSilaClient: ConfiguredSiLAClient = {
      clientName: this.configSilaClientForm.value.clientName,
      siLAClientHost: this.configSilaClientForm.value.siLAClientHost,
      siLAClientPort: this.configSilaClientForm.value.siLAClientPort,
    };

    // Call the API to update the connection configuration
    this.connectionConfigService
      .apiConnectionConfigurationPut(this.persist, configuredSilaClient)
      .subscribe({
        next: () => {
          console.log('Connection configuration updated successfully!');
        },
        error:(error) => {
          console.log('Failed to updated connection configuration!', error);
        }
      })
      .unsubscribe();
    this.configSilaClientForm.reset();
  }

  deleteConnectionConfig() {
    if (!this.configSilaClientForm.value.clientName) {
      console.error('Client name is required.');
      return;
    }
    // Call the API to delete the connection configuration
    const clientName:string = this.configSilaClientForm.value.clientName
    this.connectionConfigService
      .apiConnectionConfigurationDelete( clientName, this.remove, 'body')
      .subscribe({
        // Handle successful response
        next: () => {
          console.log('Connection configuration successfully deleted.');
          this.configSilaClientForm.reset();
        },
        // Handle error response
        error: (error) => {
          console.error('Failed to delete connection configuration:', error);
          // Handle the error appropriately (e.g., show an error message)
        },
      })
      .unsubscribe(); // Unsubscribe from the observable to prevent memory leaks
    // Reset clientName to an empty string

    this.configSilaClientForm.value.clientName.reset();
  }


}
