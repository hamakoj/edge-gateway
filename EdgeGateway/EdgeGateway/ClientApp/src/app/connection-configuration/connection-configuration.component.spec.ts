import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConnectionConfigurationComponent } from './connection-configuration.component';

describe('ConnectionConfigurationComponent', () => {
  let component: ConnectionConfigurationComponent;
  let fixture: ComponentFixture<ConnectionConfigurationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConnectionConfigurationComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ConnectionConfigurationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
