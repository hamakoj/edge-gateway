import { Component, OnInit, OnDestroy } from '@angular/core';
import {
  DynamicObjectProperty,
  FeatureDesciption,
  FeatureService,
  GatewayFeatureService,
  GatewayService,
  PropertyService,
  ServerInfo,
} from '../../../api';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

declare const window: any;

@Component({
  selector: 'app-property',
  templateUrl: './property.component.html',
  styleUrls: ['./property.component.css'],
})
export class PropertyComponent implements OnInit, OnDestroy {
  dynamicObjectProperty: DynamicObjectProperty = {};
  propertyForm!: FormGroup;

  featureDescription: FeatureDesciption = {};
  serverInfoArray: ServerInfo[] = [];
  gatewayFeatures: string[] = [];
  selectedFeature!: string;
  serviceName: string= '';
  selectedServerUuid!: string;
  property!: string;
  text:string ='';
  showTable: boolean = true;
  constructor(
    private propertyService: PropertyService,
    private featureService: FeatureService,
    private gatewayService: GatewayService,
    private gatewayFeature: GatewayFeatureService,
    public router: Router,
    public location: Location
  ) {}

  ngOnInit(): void {
    this.propertyForm = new FormGroup({
      serverId: new FormControl('', [Validators.required]),
      featureId: new FormControl('', [Validators.required]),
      propertyId: new FormControl('', [Validators.required]),
    });

    const storedData = window.localStorage.getItem('yourDataKey');
    if (storedData) {
      this.dynamicObjectProperty = JSON.parse(storedData);
    } else {
      this.postProperty(this.text);
    }
    window.localStorage.removeItem('yourDataKey');

    this.getAllServerInfo1();
  }


  extractServiceName(selectedFeature: string) {
    if (selectedFeature) {
      const parts = selectedFeature.split('/');
      this.serviceName = parts[parts.length - 2];
      console.log('extract name:', this.serviceName);
    }

    this.featureService
      .apiFeatureIdentifierGet(this.serviceName, 'body', false)
      .subscribe((response) => {
        console.log('resp feature Identifier', response);
        this.featureDescription = response;
        console.log(' show Properties', this.featureDescription.properties);
      });
    this.serviceName = '';
  }


  postProperty(text:any) {

    console.log('text ',text);
    let serverId = this.propertyForm.value.serverId;
    let featureId = this.propertyForm.value.featureId;
    let propertyId = text;

    if (featureId) {
      const parts = featureId.split('/');
      featureId = parts[parts.length - 2];
      console.log('featureId:', featureId);
    }
    this.showTable= true;
    this.propertyService
      .apiExecutionServerIdFeatureIdPropertiesPropertyIdPost(
        serverId,
        featureId,
        propertyId,
        'body',
        false,
        { httpHeaderAccept: 'application/json' }
      )
      .subscribe({
        next: (data) => {
          // this.dynamicObjectProperty = null;
          window.localStorage.removeItem('yourDataKey');
          this.dynamicObjectProperty = data;
          console.log('post pro', this.dynamicObjectProperty);
          this.storeData();
          this.ngOnDestroy();
        },
        error: (error) => {
          console.log('Failed to post property:', error);
        },
      });

  }

  getAllServerInfo1() {
    this.gatewayService.apiGatewayGet().subscribe((data) => {
      this.serverInfoArray = data;
      console.log('serverDetail:', this.serverInfoArray);
    });
    this.gatewayFeature
      .apiGatewayFeaturesGet('body', false, { httpHeaderAccept: 'text/json' })
      .subscribe({
        next: (data) => {
          this.gatewayFeatures = data;
          console.log('Gateway features:', this.gatewayFeatures);
        },
        error: (error) => {
          console.error('Failed to get gateway feature:', error);
          // Handle the error appropriately (e.g., show an error message)
        },
      });
  }

  storeData() {
    window.localStorage.setItem(
      'yourDataKey',
      JSON.stringify(this.dynamicObjectProperty)
    );
  }
  ngOnDestroy() {
    this.storeData();
  }
}
