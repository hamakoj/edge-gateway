import { Component, OnInit } from '@angular/core';
import { FeatureDesciption, FeatureService } from '../../../api';

interface Property {
  displayName: string;
  identifier: string;
  description: string;
  observable: string;
  dataType: { item: string } | null;
  definedExecutionErrors: string[];
}
interface FeatureDescription {
  identifier: string;
  properties: Property[];
}

@Component({
  selector: 'app-feature',
  templateUrl: './feature.component.html',
  styleUrls: ['./feature.component.css'],
})
export class FeatureComponent implements OnInit {
  featureDescription: FeatureDesciption = { identifier: '', properties: [] };
  f_identifier: string = '';
  addedProperties: string[] = [];

  constructor(private featureService: FeatureService) {}

  ngOnInit(): void {
    this.getFeature();
  }

  getFeature() {
    this.featureService
      .apiFeatureIdentifierGet(this.f_identifier, 'body', false)
      .subscribe((response) => {
        console.log('resp:', response);
        this.featureDescription = response;
        console.log(this.featureDescription.identifier);
      });
    this.f_identifier = '';
  }

  removeProperty(index: number) {
    this.addedProperties.splice(index, 1);
  }
}
