import { Component, OnInit } from '@angular/core';



@Component({
  selector: 'app-new-home',
  templateUrl: './new-home.component.html',
  styleUrls: ['./new-home.component.css'],
})
export class NewHomeComponent implements OnInit {
  autosize: boolean = true;
  opened: boolean = false;
  panelOpenState = false;
  isExpanded: boolean = false;
  toggleValue: boolean = true;

  constructor() {}

  ngOnInit(): void {}
}
