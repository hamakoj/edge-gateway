
import {Component, OnInit} from '@angular/core';
import {DiscoverySpec, GatewayFeatureService, GatewayService, ServerInfo, ServerSpec} from "../../../api";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import { FeatureDesciption, FeatureService } from '../../../api';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  serverInfoArray: ServerInfo[] = [];
  gatewayFeatures: Array<string> = [];

  featureDescription: FeatureDesciption = {};

  selectedFeature!: string;
  selectedStatus!: string;
  selectedServerUuid!: string;
  showTable = false;
  serviceName!: string;
  filteredServerInfoArray: ServerInfo[] = [];
  

  serverForm!: FormGroup;
  numberForm!: FormGroup;
  submitted = false;
  error = '';

  loadingInProgress: string = '';

  constructor(
    private gatewayService: GatewayService,
    private gatewayFeature: GatewayFeatureService,
    private router: Router,
    private route: ActivatedRoute,

    private featureService: FeatureService
  ) {}
  ngOnInit(): void {
    this.numberForm = new FormGroup({
      number: new FormControl('', [
        Validators.required,
        Validators.minLength(0),
        Validators.maxLength(100),
      ]),
    });
    this.serverForm = new FormGroup({
      host: new FormControl('', [Validators.required]),
      port: new FormControl('', [
        Validators.required,
        Validators.pattern('^[0-9]+'),
      ]),
    });

    this.postDiscoverySpec1();
    this.getAllServerInfo1().then();
    this.addServerSpec();
  }

  get numberFormControl() {
    return this.numberForm.controls;
  }
  get serverFormControl() {
    return this.serverForm.controls;
  }

  postDiscoverySpec1() {
    if (this.numberFormControl['number'].valid) {
      const discoverySpec: DiscoverySpec = {
        timeout: this.numberFormControl['number'].value,
      };
      this.gatewayService
        .apiGatewayPost(discoverySpec, 'body', false)
        .subscribe({
          next: () => {
            console.log('send Successfully 1');
            alert('Time out send!!');
          },
          error: (err) => {
            this.error = err.error.message;
            alert('Form  Submitted Failed!!!');
          },
        });
      this.numberForm.reset();
    }
    this.showTable = true;
  }

  async getAllServerInfo1() {
    await this.postDiscoverySpec1();
    this.gatewayService.apiGatewayGet().subscribe((data) => {
      this.serverInfoArray = data;
      console.log('serverDetail', this.serverInfoArray);
    });
    this.gatewayFeature
      .apiGatewayFeaturesGet('body', false, { httpHeaderAccept: 'text/json' })
      .subscribe({
        next: (data) => {
          this.gatewayFeatures = data;
          console.log(this.gatewayFeatures);
        },
        error: (error) => {
          console.error('Failed to get gateway feature:', error);
          // Handle the error appropriately (e.g., show an error message)
        },
      });
  }

  addServerSpec() {
    this.loadingInProgress = 'Server addition in progress';
    if (this.serverForm.valid && this.submitted) {
      const serverSpec: ServerSpec = {
        host: this.serverForm.value.host,
        port: this.serverForm.value.port,
      };
      console.log(serverSpec.host);
      console.log(serverSpec.port);

      this.gatewayService.apiGatewayPut(serverSpec).subscribe(
        (response) => {
          this.loadingInProgress = '';
          console.log(response.ServerSpec.port);
          console.log(response.ServerSpec.host);
        },
        (err) => {
          this.loadingInProgress = '';
          this.error = err.error.message;
          alert('Form Submitted Failed!!!');
        }
      );
    } else {
      console.log('form not valid');
    }
  }

  redirectToComponent() {
    this.router.navigate(
      ['execution/{serverId}/{featureId}/properties/{propertyId}'],
      { relativeTo: this.route }
    );
  }

  extractServiceName(selectedFeature: string) {
    const parts = selectedFeature.split('/');
    this.serviceName = parts[parts.length - 2];
    console.log(this.serviceName);

    this.featureService
      .apiFeatureIdentifierGet(this.serviceName, 'body', false)
      .subscribe((response) => {
        console.log('resp:', response);
        this.featureDescription = response;
        console.log(this.featureDescription.identifier);
      });
    this.serviceName = '';
  }

  onServerSelect() {
    // Toggle the table visibility based on the selected server UUID
    this.showTable =
      this.selectedServerUuid !== undefined && this.selectedServerUuid !== null;
  }
}
