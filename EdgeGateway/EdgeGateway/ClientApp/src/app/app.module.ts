import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { HomeComponent } from './home/home.component';

import { UnObservableCommandComponent } from './un-observable-command/un-observable-command.component';
import { ObservableCommandComponent } from './observable-command/observable-command.component';
import { PropertyComponent } from './property/property.component';
import { FeatureComponent } from './feature/feature.component';
import { GatewayFeatureComponent } from './gateway-feature/gateway-feature.component';
import { ConnectionConfigurationComponent } from './connection-configuration/connection-configuration.component';
import { NewHomeComponent } from './new-home/new-home.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';



@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    HomeComponent,

    UnObservableCommandComponent,
    ObservableCommandComponent,
    PropertyComponent,
    FeatureComponent,
    GatewayFeatureComponent,
    ConnectionConfigurationComponent,
    NewHomeComponent,
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,



    FormsModule,
    RouterModule.forRoot([
      { path: '', component: HomeComponent, pathMatch: 'full' },
      { path: 'feature', component: FeatureComponent },

      { path: 'gatewayFeatures', component: GatewayFeatureComponent },
      {
        path: 'connectionConfiguration',
        component: ConnectionConfigurationComponent,
      },
      {
        path: 'execution/{serverId}/{featureId}/properties/{propertyId}',
        component: PropertyComponent,
      },

      { path: 'execution', component: ObservableCommandComponent },

      {
        path: 'execution/{serverId}/{featureId}/commands/{commandId}',
        component: UnObservableCommandComponent,
      },

      { path: 'newHome', component: NewHomeComponent },
    ]),
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
