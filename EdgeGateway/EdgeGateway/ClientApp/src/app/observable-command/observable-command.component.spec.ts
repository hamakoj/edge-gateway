import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ObservableCommandComponent } from './observable-command.component';

describe('ObservableCommandComponent', () => {
  let component: ObservableCommandComponent;
  let fixture: ComponentFixture<ObservableCommandComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ObservableCommandComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ObservableCommandComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
