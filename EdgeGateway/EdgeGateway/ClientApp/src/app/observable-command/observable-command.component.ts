import { Component, OnInit } from '@angular/core';
import {
  CommandResult,
  FeatureDesciption,
  FeatureService,
  GatewayFeatureService,
  GatewayService,
  ObservableCommandExecution,
  ObservableCommandService,
  ServerInfo,
  UnobservableCommandService,
} from '../../../api';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import {HubConnection, HubConnectionBuilder, LogLevel} from '@microsoft/signalr';


@Component({
  selector: 'app-observable-command',
  templateUrl: './observable-command.component.html',
  styleUrls: ['./observable-command.component.css'],
})
export class ObservableCommandComponent implements OnInit {
  observableCommandExecution: ObservableCommandExecution = {};
  commandResult: CommandResult = {};
  commandForm!:FormGroup;


  serviceName!: string;
  selectedServerUuid!: string;
  selectedFeature!: string;
  showTable = false;


  featureDescription: FeatureDesciption = {};
  serverInfoArray: ServerInfo[] = [];
  gatewayFeatures: string[] = [];

  isPanelOpen = false;

  togglePanel(): void {
    this.isPanelOpen = !this.isPanelOpen;
  }

  constructor(
    private observableCommand: ObservableCommandService,
    private unobservableCommand: UnobservableCommandService,
    private featureService: FeatureService,
    private gatewayService: GatewayService,
    private gatewayFeature: GatewayFeatureService,
    private router: Router,
    private route: ActivatedRoute,
    private hubConnection: HubConnection
  ) {}

  ngOnInit(): void {

    // Create the SignalR connection
    this.hubConnection = new HubConnectionBuilder()
      .withUrl('http://your-signalr-hub-url') // Replace with your SignalR hub URL
      .configureLogging(LogLevel.Information)
      .build();
    // Start the connection
    this.hubConnection.start()
      .then(() => {
        console.log('SignalR connection started');
        // Subscribe to any relevant events here
      })
      .catch(error => console.error('Error starting SignalR connection:', error));

    this.hubConnection.on('CommandPosted', (serverId, featureId, commandId, body) => {
      console.log('New command posted:', serverId, featureId, commandId, body);
      // Handle the event data as needed
    });

    this.commandForm = new FormGroup({
      serverId: new FormControl('', [
        Validators.required
      ]),
      featureId: new FormControl('', [
        Validators.required
      ]),
      commandId: new FormControl('', [
        Validators.required
      ]),
      body: new FormControl('', [
        Validators.required
      ])
    });

    this.getAllServerInfo1();
  }

  getAllServerInfo1() {
    this.gatewayService.apiGatewayGet().subscribe((data) => {
      this.serverInfoArray = data;
      console.log('serverDetail', this.serverInfoArray);
    });
    this.gatewayFeature
      .apiGatewayFeaturesGet('body', false, { httpHeaderAccept: 'text/json' })
      .subscribe({
        next: (data) => {
          this.gatewayFeatures = data;
          console.log('Gateway feature:' , this.gatewayFeatures);
        },
        error: (error) => {
          console.error('Failed to get gateway feature:', error);
          // Handle the error appropriately (e.g., show an error message)
        },
      });
  }

  extractServiceName(selectedFeature: string) {
    const parts = selectedFeature.split('/');
    this.serviceName = parts[parts.length - 2];
    console.log('extract name:', this.serviceName);

    this.featureService
      .apiFeatureIdentifierGet(this.serviceName, 'body', false)
      .subscribe((response) => {
        console.log('resp:', response);
        this.featureDescription = response;
        console.log('show command:', this.featureDescription.commands);
      });
    this.serviceName = '';
  }

  postCommand(){
    const serverId = this.commandForm.value.serverId;
    const featureId = this.commandForm.value.featureId;
    const commandId = this.commandForm.value.commandId;
    const body:string = this.commandForm.value.body;

if (this.featureDescription.commands && this.featureDescription.commands[0].observable === 1){

  this.unobservableCommand.apiExecutionServerIdFeatureIdCommandsCommandIdPost(serverId, featureId, commandId, body, "body", false, {httpHeaderAccept:"application/json"})
    .subscribe({
      next: (data) =>{
        this.commandResult = data;
        console.log('unobservable command:', this.commandResult);
      },
      error:(error) => {
        console.log('Failed to post property 1:', error)
      },
    });
}
else{
  this.observableCommand.apiExecutionServerIdFeatureIdObservablecommandsCommandIdPost(serverId, featureId, commandId, body, "body", false, {httpHeaderAccept:"application/json"})
    .subscribe({
      next: (data) =>{
        this.observableCommandExecution = data;
        console.log('observable command:', this.observableCommandExecution);
      },
      error:(error) => {
        console.log('Failed to post property 2:', error)
      },
    });
  // Call the SignalR hub method to notify other clients
  this.hubConnection.invoke('NotifyCommandPosted', serverId, featureId, commandId, body)
    .catch(error => console.error('Failed to invoke NotifyCommandPosted:', error));
}
  }

  redirectToComponent() {
    this.router.navigate(
      ['execution/{serverId}/{featureId}/properties/{propertyId}'],
      { relativeTo: this.route }
    );
  }
  onServerSelect() {
    // Toggle the table visibility based on the selected server UUID
    this.showTable =
      this.selectedServerUuid !== undefined && this.selectedServerUuid !== null;
  }
}
