﻿using Tecan.Sila2;
using Tecan.Sila2.Discovery;
using Tecan.Sila2.DynamicClient;
using TestClient.GatewayService;

var executionManager = new DiscoveryExecutionManager();
var connector = new ServerConnector(executionManager);
var discovery = new ServerDiscovery(connector);

var servers = discovery.GetServers(TimeSpan.FromSeconds(10));

Console.WriteLine("Actual servers in the network");
foreach (var server in servers)
{
    Console.WriteLine(server.Config.Name);
}

var gateway = servers.Single(s => s.Features.Any(f => f.FullyQualifiedIdentifier == "org.silastandard/gateway/GatewayService/v1"));
var gatewayClient = new GatewayServiceClient(gateway.Channel, executionManager);

void ShowConnectedServers()
{
    var connectedServers = gatewayClient!.ConnectedServers ?? Enumerable.Empty<TestClient.Server>();
    Console.WriteLine($"{connectedServers.Count()} connected servers");

    foreach (var server in connectedServers)
    {
        Console.WriteLine($" - {server.Name} ({server.ServerUuid})");

        if (server.Features.Contains("sila.hackathon/demo/GreetingService/v1") && server.ServerUuid != gateway.Config.Uuid.ToString())
        {
            var feature = gateway.Features.Single(f => f.FullyQualifiedIdentifier == "sila.hackathon/demo/GreetingService/v1");
            var command = feature.Items.OfType<FeatureCommand>().Single(c => c.Identifier == "SayHello");
            var commandClient = new NonObservableCommandClient(command, new FeatureContext(feature, gateway, executionManager));
            var request = commandClient.CreateRequest();
            request.Value = new DynamicObject
            {
                Elements =
                    {
                        new DynamicObjectProperty(command.Parameter[0]) { Value = "Gateway" }
                    }
            };
            try
            {
                var response = commandClient.Invoke(request, new Dictionary<string, byte[]>
                {
                    ["org.silastandard/gateway/GatewayService/v1/Metadata/ServerUuid"] = ByteSerializer.ToByteArray(new PropertyResponse<StringDto>(server.ServerUuid))
                });
                Console.WriteLine("  " + response.ToString());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
    }
}

ShowConnectedServers();
gatewayClient.PropertyChanged += (sender, e) => ShowConnectedServers();

gatewayClient.ScanNetwork(5);

Console.ReadLine();