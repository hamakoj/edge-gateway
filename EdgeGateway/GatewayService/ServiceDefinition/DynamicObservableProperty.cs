﻿using Hsrm.EdgeGateway.Contracts;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tecan.Sila2;
using Tecan.Sila2.DynamicClient;

namespace Hsrm.EdgeGateway.ServiceDefinition
{
    internal class DynamicObservableProperty : DynamicServiceDefinition, INotifyPropertyChanged
    {
        private Feature _featureDefinition;
        private FeatureProperty _property;

        public DynamicObservableProperty(Feature featureDefinition, FeatureProperty property, IServerContextProvider contextProvider)
            : base(contextProvider)
        {
            _featureDefinition = featureDefinition;
            _property = property;
        }

        public static DynamicObjectProperty Value { get; internal set; }

        public event PropertyChangedEventHandler PropertyChanged;

        internal string CreateClientSerializer(DynamicObjectSerializer serializer)
        {
            throw new NotImplementedException();
        }

        internal DynamicObjectProperty LastReceived()
        {
            return Value;
        }
    }
}
