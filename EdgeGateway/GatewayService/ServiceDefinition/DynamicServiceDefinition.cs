﻿using Hsrm.EdgeGateway.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tecan.Sila2;
using Tecan.Sila2.DynamicClient;

namespace Hsrm.EdgeGateway.ServiceDefinition
{
    internal abstract class DynamicServiceDefinition
    {
        protected readonly IServerContextProvider _contextProvider;

        protected DynamicServiceDefinition(IServerContextProvider contextProvider)
        {
            _contextProvider = contextProvider;
        }
    }
}
