﻿using Hsrm.EdgeGateway.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tecan.Sila2;
using Tecan.Sila2.DynamicClient;

namespace Hsrm.EdgeGateway.ServiceDefinition
{
    internal class DynamicObservableCommand : DynamicServiceDefinition
    {
        private readonly Feature _featureDefinition;
        private readonly FeatureCommand _command;

        public DynamicObservableCommand(Feature featureDefinition, FeatureCommand command, IServerContextProvider contextProvider)
            : base(contextProvider)
        {
            _featureDefinition = featureDefinition;
            _command = command;
        }

        public IObservableCommand<DynamicObjectProperty> Invoke(DynamicRequest request)
        {
            var context = _contextProvider.Context;
            var commandClient = new ObservableCommandClient(_command, new FeatureContext(_featureDefinition, context.ConnectedServer.Server, context.ConnectedServer.ExecutionManager));
            return commandClient.Invoke(request, context.ConnectedServer.CreateAdditionalMetadata(context.Metadata));
        }
    }
}
