﻿using Hsrm.EdgeGateway.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Tecan.Sila2;
using Tecan.Sila2.DynamicClient;
using Tecan.Sila2.Server;

namespace Hsrm.EdgeGateway.ServiceDefinition
{
    internal class DynamicFeatureProvider : IFeatureProvider
    {
        public Feature FeatureDefinition { get; }

        private readonly IServerContextProvider _contextProvider;
        private readonly DynamicObjectSerializer _serializer;
        private readonly ISiLAServer _server;

        public DynamicFeatureProvider(Feature featureDefinition, IServerContextProvider contextProvider, ISiLAServer server)
        {
            FeatureDefinition = featureDefinition;
            _contextProvider = contextProvider;
            _server = server;
            _serializer = new DynamicObjectSerializer(ResolveType);
        }

        private byte[] Serialize(DynamicObjectProperty arg)
        {
            return _serializer.Serialize(arg, _server);
        }

        private byte[] Serialize(DynamicRequest request)
        { 
            return _serializer.Serialize(request, _server);
        }

        private DynamicObjectProperty DeserializeAsExpected(byte[] bytes, DataTypeType expected, bool encapsulate)
        {
            var property = new DynamicObjectProperty(string.Empty, string.Empty, string.Empty, expected);
            _serializer.Deserialize(property, bytes, encapsulate, _server);
            return property;
        }

        private DynamicRequest DeserializeAsRequest(byte[] bytes, FeatureCommand command)
        {
            var request = new DynamicRequest(command, FeatureDefinition.GetFullyQualifiedIdentifier(command), new DataTypeType
            {
                Item = new StructureType
                {
                    Element = command.Parameter
                }
            });
            _serializer.Deserialize(request, bytes, false, _server);
            return request;
        }

        private ByteSerializer<DynamicObjectProperty> CreateSerializer(DataTypeType expectedType, bool encapsulate)
        {
            return new ByteSerializer<DynamicObjectProperty>(Serialize, bytes => DeserializeAsExpected(bytes, expectedType, encapsulate));
        }

        private ByteSerializer<DynamicRequest> CreateRequestSerializer(FeatureCommand command)
        {
            return new ByteSerializer<DynamicRequest>(Serialize, bytes => DeserializeAsRequest(bytes, command));
        }

        private DataTypeType ResolveType(string type)
        {
            return FeatureDefinition.Items.OfType<SiLAElement>().SingleOrDefault(t => t.Identifier == type)?.DataType;
        }

        public MethodInfo GetCommand(string commandIdentifier)
        {
            return null;
        }

        public PropertyInfo GetProperty(string propertyIdentifier)
        {
            return null;
        }

        public void Register(IServerBuilder registration)
        {
            foreach (var command in FeatureDefinition.Items.OfType<FeatureCommand>())
            {
                if (command.Observable == FeatureCommandObservable.Yes)
                {
                    RegisterObservableCommand(command, registration);
                }
                else
                {
                    RegisterUnobservableCommand(command, registration);
                }
            }
            foreach (var property in FeatureDefinition.Items.OfType<FeatureProperty>())
            {
                if (property.Observable == FeaturePropertyObservable.Yes)
                {
                    RegisterObservableProperty(property, registration);
                }
                else
                {
                    RegisterUnobservableProperty(property, registration);
                }
            }
        }

        private void RegisterUnobservableProperty(FeatureProperty property, IServerBuilder registration)
        {
            var propertyClient = new DynamicUnobservableProperty(FeatureDefinition, property, _contextProvider);
            registration.RegisterUnobservableProperty(property.Identifier, propertyClient.Invoke, CreateSerializer(property.DataType, true));
        }

        private void RegisterObservableProperty(FeatureProperty property, IServerBuilder registration)
        {
            var propertyClient = new DynamicObservableProperty(FeatureDefinition, property, _contextProvider);
            registration.RegisterObservableProperty(property.Identifier, propertyClient.LastReceived, propertyClient, CreateSerializer(property.DataType, true), nameof(DynamicObservableProperty.Value));
        }

        private void RegisterUnobservableCommand(FeatureCommand command, IServerBuilder registration)
        {
            var commandClient = new DynamicUnobservableCommand(FeatureDefinition, command, _contextProvider);
            registration.RegisterUnobservableCommand(command.Identifier, commandClient.Invoke, CreateRequestSerializer(command), CreateSerializer(new DataTypeType
            {
                Item = new StructureType() { Element = command.Response }
            }, false));
        }

        private void RegisterObservableCommand(FeatureCommand command, IServerBuilder registration)
        {
            var commandClient = new DynamicObservableCommand(FeatureDefinition, command, _contextProvider);
            registration.RegisterObservableCommand(command.Identifier, commandClient.Invoke, ConvertResponse, ConvertException, CreateRequestSerializer(command), CreateSerializer(new DataTypeType
            {
                Item = new StructureType() { Element = command.Response }
            }, false));
        }

        private Exception ConvertException(Exception ex)
        {
            return ex;
        }

        private DynamicObjectProperty ConvertResponse(DynamicObjectProperty arg)
        {
            return arg;
        }
    }
}
