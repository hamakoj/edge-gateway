using Common.Logging;
using Hsrm.EdgeGateway;
using Hsrm.EdgeGateway.Contracts;
using Hsrm.EdgeGateway.Controllers;
using Hsrm.EdgeGateway.HubConfig;
using Hsrm.EdgeGateway.Services;
using Microsoft.Extensions.DependencyInjection;
using Tecan.Sila2;
using Tecan.Sila2.Client;
using Tecan.Sila2.Client.ExecutionManagement;
using Tecan.Sila2.Discovery;
using Tecan.Sila2.Logging;
using Tecan.Sila2.Server;
using Tecan.Sila2.ServerPooling;

var builder = WebApplication.CreateBuilder(args);
var config = new ServiceConfigurationBuilder(new NoCertificateProvider());
var pool = new PoolServer(config);

// Add services to the container.
config.Configure("localhost", 7025);
builder.WebHost.UseKestrel(options => config.ConfigureKestrel(options, Microsoft.AspNetCore.Server.Kestrel.Core.HttpProtocols.Http1AndHttp2));

builder.Services.AddControllersWithViews()
                .AddApplicationPart(typeof(FeatureController).Assembly);
builder.Services.AddSila2ServerPool();

builder.Services.AddSingleton<IServerConnector>(sp => new ServerConnector(new DiscoveryExecutionManager()));
builder.Services.AddSingleton<IServerDiscovery, ServerDiscovery>();
builder.Services.AddSingleton<IFeatureResolver, PoolFeatureResolver>();
builder.Services.AddSingleton<IServerResolver, PoolServerResolver>();
builder.Services.AddSingleton<IServerPool>(_ => pool);
builder.Services.AddSingleton<ICommandRegistry, HubCommandRegistry>();
builder.Services.AddSingleton<IPropertySubscriptionHandler, PropertySubscriptionHandler>();
builder.Services.AddSingleton<IExecutionManagerFactory, ExecutionManagerFactory>();
builder.Services.AddSingleton<IServiceConfigurationBuilder<IServerPool>>(_ => config);

builder.Services.AddSignalR();

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddCors(options =>
{
    options.AddPolicy("ProxyCorsPolicy", builder => builder
        .WithOrigins("https://localhost:44469")
        .AllowAnyMethod()
    .AllowAnyHeader()
    .AllowCredentials());
});

var app = builder.Build();

var loggerFactory = app.Services.GetRequiredService<ILoggerFactory>();
LogManager.Adapter = new AspNetAdapter(loggerFactory);

var logger = loggerFactory.CreateLogger("Pool");

pool.ServerConnected += (o, e) =>
{
    logger.LogInformation($"Server {e.Server.Config.Name} ({e.Server.Config.Uuid}) connected");
};

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}
else
{
    app.UseCors("ProxyCorsPolicy");
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();
app.UseStaticFiles();
app.UseRouting();


app.MapControllerRoute(
    name: "default",
    pattern: "{controller}/{action=Index}/{id?}");

app.MapSila2ServerPool();

app.MapHub<GatewayHub>("/hub");

app.MapFallbackToFile("index.html"); ;

app.Run();

