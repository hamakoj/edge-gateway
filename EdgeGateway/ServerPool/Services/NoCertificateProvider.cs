﻿using Tecan.Sila2.Security;

namespace Hsrm.EdgeGateway.Services
{
    public class NoCertificateProvider : IServerCertificateProvider
    {
        public CertificateContext? CreateContext(Guid serverGuid)
        {
            return null;
        }

        public CertificateContext? CreateContext()
        {
            return null;
        }
    }
}
