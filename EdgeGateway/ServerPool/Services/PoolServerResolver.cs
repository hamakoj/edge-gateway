﻿using Hsrm.EdgeGateway.Contracts;
using Hsrm.EdgeGateway.GatewayService;
using Tecan.Sila2.Client;

namespace Hsrm.EdgeGateway.Services
{
    public class PoolServerResolver : IServerResolver
    {
        private readonly IServerPool _serverPool;
        private readonly IExecutionManagerFactory _executionManagerFactory;

        public PoolServerResolver(IServerPool serverPool, IExecutionManagerFactory executionManagerFactory)
        {
            _serverPool = serverPool;
            _executionManagerFactory = executionManagerFactory;
        }

        public ConnectedServer? ResolveServer(string serverUuid)
        {
            if (Guid.TryParse(serverUuid, out var serverId))
            {
                var server = _serverPool.ConnectedServers.FirstOrDefault(s => s.Config.Uuid == serverId);
                if (server != null)
                {
                    return new ConnectedServer(server, _executionManagerFactory.CreateExecutionManager(server));
                }
            }
            return null;
        }
    }
}
