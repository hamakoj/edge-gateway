﻿using Tecan.Sila2.DynamicClient;

namespace Hsrm.EdgeGateway.Contracts
{
    public interface IPropertySubscriptionHandler
    {
        Task<DynamicObjectProperty?> Subscribe(PropertySubscription subscription);

        IEnumerable<PropertySubscription> GetSubscriptions(string serverUuid, string featureId);

        bool CancelSubscription(PropertySubscription subscription);
    }
}
