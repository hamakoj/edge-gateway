﻿namespace Hsrm.EdgeGateway.Contracts
{
    /// <summary>
    /// Denotes an observable command execution
    /// </summary>
    /// <param name="ExecutionUUID">The unique identifier of the command execution</param>
    /// <param name="ErrorMessage">The error message explaining why the command could not be started</param>
    public record ObservableCommandExecution(
        string? ExecutionUUID,
        string? ErrorMessage);
}
