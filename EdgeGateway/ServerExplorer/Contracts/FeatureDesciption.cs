﻿using Tecan.Sila2;

namespace Hsrm.EdgeGateway.Contracts
{
    /// <summary>
    /// Denotes a SiLA2 feature
    /// </summary>
    /// <param name="Identifier">The identifier of the feature</param>
    /// <param name="DisplayName">A human-readable name of the feature</param>
    /// <param name="Version">The feature version</param>
    /// <param name="Description">A human-readable description of the feature</param>
    /// <param name="Category">The feature category</param>
    /// <param name="Originator">The originator of the feature</param>
    /// <param name="Maturity">Denotes whether the feature is draft, verified or normative</param>
    /// <param name="Commands">The commands provided by this feature</param>
    /// <param name="Properties">The properties provided by this feature</param>
    /// <param name="DataTypes">The custom data types provided by this feature</param>
    /// <param name="Metadata">The client metadata provided by this feature</param>
    /// <param name="DefinedExecutionErrors">Execution errors defined by this feature</param>
    public record FeatureDesciption(
        string Identifier,
        string DisplayName,
        string Version,
        string Description,
        string Category,
        string Originator,
        FeatureMaturityLevel Maturity,
        IReadOnlyList<FeatureCommand> Commands,
        IReadOnlyList<FeatureProperty> Properties,
        IReadOnlyList<SiLAElement> DataTypes,
        IReadOnlyList<FeatureMetadata> Metadata,
        IReadOnlyList<FeatureDefinedExecutionError> DefinedExecutionErrors);
}
