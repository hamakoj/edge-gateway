﻿namespace Hsrm.EdgeGateway.Contracts
{
    /// <summary>
    /// Denotes a defined error
    /// </summary>
    /// <param name="Identifier">The fully qualified identifier of the error</param>
    /// <param name="Message">The error message</param>
    public record DefinedError(string Identifier, string Message);
}
