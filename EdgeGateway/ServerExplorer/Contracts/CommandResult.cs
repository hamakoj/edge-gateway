﻿using Tecan.Sila2.DynamicClient;

namespace Hsrm.EdgeGateway.Contracts
{
    /// <summary>
    /// Denotes the result of a command
    /// </summary>
    /// <param name="Result">The actual result or null</param>
    /// <param name="DefinedError">The error that has happened or null</param>
    /// <param name="UndefinedError">An undefined error that has happened or null</param>
    public record CommandResult(
        DynamicObjectProperty? Result,
        DefinedError? DefinedError,
        UndefinedError? UndefinedError);
}
