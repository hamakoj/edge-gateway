﻿namespace Hsrm.EdgeGateway.Contracts
{
    /// <summary>
    /// An undefined error that occurred during command execution
    /// </summary>
    /// <param name="Type">The type of the error</param>
    /// <param name="Message">The error message</param>
    public record UndefinedError(string Type, string Message);
}
