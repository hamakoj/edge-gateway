﻿using Hsrm.EdgeGateway.Contracts;
using Hsrm.EdgeGateway.HubConfig;
using Microsoft.AspNetCore.SignalR;
using Tecan.Sila2;
using Tecan.Sila2.DynamicClient;

namespace Hsrm.EdgeGateway.Services
{
    public class HubCommandRegistry : ICommandRegistry
    {
        private readonly IHubContext<GatewayHub> _hubContext;

        public HubCommandRegistry(IHubContext<GatewayHub> hubContext)
        {
            _hubContext = hubContext;
        }

        public void RegisterCommand(DynamicCommand commandExecution)
        {
            Task.Run(() => ListenToStateUpdates(commandExecution));
            if (commandExecution is DynamicIntermediateCommand intermediateCommand)
            {
                Task.Run(() => ListenToIntermediateResults(intermediateCommand));
            }
        }

        private async Task ListenToStateUpdates(DynamicCommand commandExecution)
        {
            var executionIdString = $"/commandexecution/{commandExecution.CommandId.CommandId}/state";
            while (await commandExecution.StateUpdates.WaitToReadAsync())
            {
                if (commandExecution.StateUpdates.TryRead(out var stateUpdate))
                {
                    await _hubContext.Clients.All.SendAsync(executionIdString, stateUpdate);
                }
            }
            var resultChannel = $"/commandexecution/{commandExecution.CommandId.CommandId}/result";
            try
            {
                var result = await commandExecution.Response;
                await _hubContext.Clients.All.SendAsync(resultChannel, new CommandResult(result, null, null));
            }
            catch (DefinedErrorException definedError)
            {
                await _hubContext.Clients.All.SendAsync(resultChannel, new CommandResult(null, new DefinedError(definedError.ErrorIdentifier, definedError.Message), null));
            }
            catch (Exception exception)
            {
                await _hubContext.Clients.All.SendAsync(resultChannel, new CommandResult(null, null, new UndefinedError(exception.GetType().Name, exception.Message)));
            }
        }



        private async Task ListenToIntermediateResults(DynamicIntermediateCommand commandExecution)
        {
            var executionIdString = $"/commandexecution/{commandExecution.CommandId.CommandId}/intermediates";
            while (await commandExecution.IntermediateValues.WaitToReadAsync())
            {
                if (commandExecution.IntermediateValues.TryRead(out var intermediateResults))
                {
                    await _hubContext.Clients.All.SendAsync(executionIdString, intermediateResults);
                }
            }
        }
    }
}
