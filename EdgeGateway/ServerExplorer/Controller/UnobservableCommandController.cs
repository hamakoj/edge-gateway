﻿using Hsrm.EdgeGateway.Contracts;
using Microsoft.AspNetCore.Mvc;
using System.Text.Json;
using Tecan.Sila2;
using Tecan.Sila2.DynamicClient;

namespace Hsrm.EdgeGateway.Controllers
{
    [ApiController]
    [Route("/api/execution/{serverId}/{featureId}/commands/{commandId}")]
    public class UnobservableCommandController : Controller
    {
        private readonly IFeatureResolver _featureResolver;
        private readonly IServerResolver _serverResolver;

        public UnobservableCommandController(IFeatureResolver featureResolver, IServerResolver serverResolver)
        {
            _featureResolver = featureResolver;
            _serverResolver = serverResolver;
        }

        [HttpPost]
        public async Task<ActionResult<CommandResult>> Invoke(string serverId, string featureId, string commandId, [FromBody] JsonDocument parameters)
        {
            var server = _serverResolver.ResolveServer(serverId);
            var feature = _featureResolver.Resolve(featureId);

            var command = feature.Items.OfType<FeatureCommand>()
                .FirstOrDefault(c => string.Equals(c.Identifier, commandId, StringComparison.OrdinalIgnoreCase));

            if (command == null)
            {
                return BadRequest("Command not found");
            }
            if (command.Observable == FeatureCommandObservable.Yes)
            {
                return BadRequest("Command is observable");
            }

            var commandClient = new NonObservableCommandClient(command, new FeatureContext(feature, server.Server, server.ExecutionManager));
            var request = commandClient.CreateRequest();
            try
            {
                CommandHelper.FillRequest(request, parameters);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            try
            {
                var response = await commandClient.InvokeAsync(request);
                return new CommandResult(response, null, null);
            }
            catch (DefinedErrorException definedError)
            {
                return new CommandResult(null, new DefinedError(definedError.ErrorIdentifier, definedError.Message), null);
            }
            catch (Exception silaException)
            {
                return new CommandResult(null, null, new UndefinedError(silaException.GetType().Name, silaException.Message));
            }
        }
    }
}
