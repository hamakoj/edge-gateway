﻿using Hsrm.EdgeGateway.Contracts;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Tecan.Sila2.DynamicClient;
using Tecan.Sila2;

namespace Hsrm.EdgeGateway.Controllers
{
    [Route("/api/execution/{serverId}/{featureId}/observableproperties")]
    [ApiController]
    public class ObservablePropertiesController : ControllerBase
    {
        private readonly IPropertySubscriptionHandler _propertySubscriptionHandler;

        public ObservablePropertiesController(IPropertySubscriptionHandler propertySubscriptionHandler)
        {
            _propertySubscriptionHandler = propertySubscriptionHandler;
        }

        [HttpGet]
        public IEnumerable<PropertySubscription> GetSubscriptions(string serverId, string featureId)
        {
            return _propertySubscriptionHandler.GetSubscriptions(serverId, featureId);
        }

        [HttpPost("{propertyId}")]
        public async Task<ActionResult<DynamicObjectProperty>> Subscribe(string serverId, string featureId, string propertyId)
        {
            var subscription = new PropertySubscription(serverId, featureId, propertyId);
            var result = await _propertySubscriptionHandler.Subscribe(subscription);
            return result == null ? NotFound() : result;
        }

        [HttpDelete("{propertyId}")]
        public ActionResult CancelSubscription(string serverId, string featureId, string propertyId)
        {
            var subscription = new PropertySubscription(serverId, featureId, propertyId);
            return _propertySubscriptionHandler.CancelSubscription(subscription) ? Ok() : NotFound();
        }
    }
}
