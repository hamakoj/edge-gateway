﻿using Hsrm.EdgeGateway.Contracts;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.Design;
using System.Text.Json;
using Tecan.Sila2;
using Tecan.Sila2.DynamicClient;

namespace Hsrm.EdgeGateway.Controllers
{
    [ApiController]
    [Route("/api/execution/{serverId}/{featureId}/properties/{propertyId}")]
    public class PropertyController : Controller
    {
        private readonly IFeatureResolver _featureResolver;
        private readonly IServerResolver _serverResolver;

        public PropertyController(IFeatureResolver featureResolver, IServerResolver serverResolver)
        {
            _featureResolver = featureResolver;
            _serverResolver = serverResolver;
        }

        [HttpPost]
        public ActionResult<DynamicObjectProperty> Get(string serverId, string featureId, string propertyId)
        {

            var server = _serverResolver.ResolveServer(serverId);
            var feature = _featureResolver.Resolve(featureId);
            var property = feature.Items.OfType<FeatureProperty>()
                .FirstOrDefault(p => string.Equals(p.Identifier, propertyId, StringComparison.OrdinalIgnoreCase));

            if (property == null)
            {
                return BadRequest("Property not found");
            }

            var propertyClient = new PropertyClient(property, new FeatureContext(feature, server.Server, server.ExecutionManager));
            return propertyClient.RequestValue();
        }
    }
}
