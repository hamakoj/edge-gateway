﻿using Hsrm.EdgeGateway.Contracts;
using Microsoft.AspNetCore.Mvc;
using Tecan.Sila2;
using Tecan.Sila2.Discovery;
using Tecan.Sila2.Server;

namespace Hsrm.EdgeGateway.Controllers
{
    [ApiController]
    [Route("/api/feature/{identifier}")]
    public class FeatureController : Controller
    {
        private readonly IFeatureResolver _featureResolver;

        public FeatureController(IFeatureResolver featureResolver)
        {
            _featureResolver = featureResolver;
        }

        [HttpGet]
        public ActionResult<FeatureDesciption> GetFeature(string identifier)
        {
            var feature = _featureResolver.Resolve(identifier);
            if (feature == null)
            {
                return NotFound();
            }
            return new FeatureDesciption(
                feature.FullyQualifiedIdentifier,
                feature.DisplayName,
                feature.FeatureVersion,
                feature.Description,
                feature.Category,
                feature.Originator,
                feature.MaturityLevel,
                feature.Items.OfType<FeatureCommand>().ToList(),
                feature.Items.OfType<FeatureProperty>().ToList(),
                feature.Items.OfType<SiLAElement>().ToList(),
                feature.Items.OfType<FeatureMetadata>().ToList(),
                feature.Items.OfType<FeatureDefinedExecutionError>().ToList());
        }
    }
}
